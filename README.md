<p align="center">
	<img alt="logo" src="https://oscimg.oschina.net/oscnet/up-d3d0a9303e11d522a06cd263f3079027715.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">RuoYi v3.8.5</h1>
<h4 align="center">基于RuoYi-Vue改造的SqlServer+MybatisPlus版</h4>
<p align="center">
	<a href="https://gitee.com/y_project/RuoYi-Vue/stargazers"><img src="https://gitee.com/y_project/RuoYi-Vue/badge/star.svg?theme=dark"></a>
	<a href="https://gitee.com/y_project/RuoYi-Vue"><img src="https://img.shields.io/badge/RuoYi-v3.8.5-brightgreen.svg"></a>
	<a href="https://gitee.com/y_project/RuoYi-Vue/blob/master/LICENSE"><img src="https://img.shields.io/github/license/mashape/apistatus.svg"></a>
</p>

### 更改项
  * 集成了SqlServer
  * 集成了MybatisPlus
  * 集成了Hutool工具包
  * 集成了Lombok包
  * 根据MybatisPlus优化了代码生成器
  * 新增、修改语句自动填充创建时间和修改时间

集成插件  
<table>
<tr>
<th>插件</th>
<th>版本</th>
</tr>
<tr>
<td>MybatisPlus</td>
<td>3.5.2</td>
</tr>
<tr>
<td>Hutool</td>
<td>5.8.12</td>
</tr>
<tr>
<td>Lombok</td>
<td>1.18.24</td>
</tr>
</table>
